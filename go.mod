module gitlab.com/Pupan-cpe/go-echo

go 1.16

require (
	github.com/labstack/echo/v4 v4.6.1 // indirect
	gorm.io/driver/mysql v1.1.3 // indirect
	gorm.io/gorm v1.22.2 // indirect
)
